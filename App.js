import { Button, StyleSheet, Text, View } from 'react-native';
import axios from 'axios';
import { useState } from 'react';

export default function App() {
  const [data, setdata] = useState(null)
  const GetApi = () => {
    axios.get("https://datacoba-api.herokuapp.com/data")
    .then(result => {
      // console.log(result.data[0].data);
      setdata(result);
    })
  }

  return (
    <View style={styles.container}>
      <Text>My App</Text>
      {data ? 
      <Text>Ini Data Saya {data.data[1].data} dengan id {data.data[1].id}</Text>
      :null}
      <Button onPress={() => GetApi()} title="Get Api" />
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
